package com.indrayana.bottomnavigationviewpager.fragment;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.tabs.TabLayout;
import com.indrayana.bottomnavigationviewpager.adapter.HomeViewPagerAdapter;
import com.indrayana.bottomnavigationviewpager.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    private static String TAB_POSITION_KEY = "TAB_POSITION_KEY";

    private ViewPager mViewPager;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mViewPager = view.findViewById(R.id.view_pager);
        mViewPager.setOffscreenPageLimit(3);

        setupViewPager(mViewPager);

        final TabLayout tabLayout = view.findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(mViewPager);    //setting tab over viewpager

        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                tabLayout.setupWithViewPager(mViewPager);
            }
        });

        if (getArguments() != null) {
            mViewPager.setCurrentItem(getArguments().getInt(TAB_POSITION_KEY));
        }

        //Implementing tab selected listener over tablayout
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                //setting current selected item over viewpager
                Bundle bundle = new Bundle();
                bundle.putInt(TAB_POSITION_KEY, tab.getPosition());
                setArguments(bundle);

                if (getArguments() != null) {
                    mViewPager.setCurrentItem(getArguments().getInt(TAB_POSITION_KEY));
                } else {
                    mViewPager.setCurrentItem(tab.getPosition());
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {
        HomeViewPagerAdapter viewPagerAdapter = new HomeViewPagerAdapter(getChildFragmentManager(), FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        for (int i=0; i<10; i++) {
            viewPagerAdapter.addFragment(HomeFragmentContent.newInstance(Integer.toString(i)), "Fragment " +i);
        }
        viewPager.setAdapter(viewPagerAdapter);
    }
}
