package com.indrayana.bottomnavigationviewpager.fragment;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indrayana.bottomnavigationviewpager.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class LiveTVFragment extends Fragment {


    public LiveTVFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_live_tv, container, false);
    }

}
