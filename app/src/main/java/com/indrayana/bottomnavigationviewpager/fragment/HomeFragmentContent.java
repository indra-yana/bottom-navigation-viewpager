package com.indrayana.bottomnavigationviewpager.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.indrayana.bottomnavigationviewpager.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragmentContent extends Fragment {

    private static final String BUNDLE_ARGS_1 = "BUNDLE_ARGS_1";

    public HomeFragmentContent() {
        // Required empty public constructor
    }

    public static HomeFragmentContent newInstance(String args) {
        HomeFragmentContent dynamicFragment = new HomeFragmentContent();

        Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_ARGS_1, args);

        dynamicFragment.setArguments(bundle);

        return dynamicFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home_content, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView textView = view.findViewById(R.id.tv_fragment_title);
        String title = getResources().getString(R.string.home);

        if (getArguments() != null) {
            title += " " +getArguments().getString(BUNDLE_ARGS_1);
        }

        textView.setText(title);
    }
}
