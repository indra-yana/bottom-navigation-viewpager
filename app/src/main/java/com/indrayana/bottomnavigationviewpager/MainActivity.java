package com.indrayana.bottomnavigationviewpager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.indrayana.bottomnavigationviewpager.adapter.ViewPagerAdapter;
import com.indrayana.bottomnavigationviewpager.fragment.HomeFragment;
import com.indrayana.bottomnavigationviewpager.fragment.LiveTVFragment;
import com.indrayana.bottomnavigationviewpager.fragment.MoviesFragment;
import com.indrayana.bottomnavigationviewpager.fragment.SettingsFragment;

public class MainActivity extends AppCompatActivity {

    private CustomViewPager viewPager;
    private BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Use For View Pager 1
        bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.home_menu:
                        viewPager.setCurrentItem(0);
                        return true;
                    case R.id.live_tv_menu:
                        viewPager.setCurrentItem(1);
                        return true;
                    case R.id.movies_menu:
                        viewPager.setCurrentItem(2);
                        return true;
                    case R.id.settings_menu:
                        viewPager.setCurrentItem(3);
                        return true;
                }
                return false;
            }
        });

        viewPager = findViewById(R.id.viewpager);
        viewPager.setSwipeEnabled(false);

        setupViewPager(viewPager);

        viewPager.setCurrentItem(0);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                setSelectedBottomNavigation(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);

        viewPagerAdapter.addFragment(new HomeFragment(), getString(R.string.home));
        viewPagerAdapter.addFragment(new LiveTVFragment(), getString(R.string.live_tv));
        viewPagerAdapter.addFragment(new MoviesFragment(), getString(R.string.movies));
        viewPagerAdapter.addFragment(new SettingsFragment(), getString(R.string.settings));

        viewPager.setAdapter(viewPagerAdapter);
    }

    @Override
    public void onBackPressed() {
        if (viewPager.getCurrentItem() > 0) {
            //if any tab selected instead of tab 1
            int position = viewPager.getCurrentItem() - 1;

            viewPager.setCurrentItem(position);
            setSelectedBottomNavigation(position);
        } else if (viewPager.getCurrentItem() == 0) {
            //if tab 0 selected
            super.onBackPressed();
        }
    }

    private void setSelectedBottomNavigation(int position) {
        switch (position) {
            case 0:
                bottomNavigationView.setSelectedItemId(R.id.home_menu);
                break;
            case 1:
                bottomNavigationView.setSelectedItemId(R.id.live_tv_menu);
                break;
            case 2:
                bottomNavigationView.setSelectedItemId(R.id.movies_menu);
                break;
            case 3:
                bottomNavigationView.setSelectedItemId(R.id.settings_menu);
                break;
        }
    }
}
